package SeleniumUtil;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

//Take a Screenshot from UI and store in System Explorer
public class ScreenShotUtil {

	public void captureSnapShot(WebDriver driver, String screenShotName) {

		try {
			// Creating a File object to take a screenshot
			File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// store source file in a new file "Screenshot"
			FileUtils.copyFile(scr, new File(".\\Screenshot\\" + screenShotName + ".Jepg"));
			Thread.sleep(2000);
			System.out.println("Screenshot was taken");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception while screenshot was taken" + e.getMessage());
		}
	}

}
