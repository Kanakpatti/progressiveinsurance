package progressiveAutoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnterZipCode {
	// Create instacne/objects for Webdriver
	public WebDriver driver;

	// Constructor for create a object of this class
	public EnterZipCode(WebDriver driver) {
		this.driver = driver;

		// Initialize webelement for this class
		PageFactory.initElements(driver, this);

	}
	// Locating all elements for using Enter zip code page

	// Anotationn
	@FindBy(xpath = "//input[@id='zipCode_overlay']")
	WebElement ZipCodeField;

	@FindBy(xpath = "//input[@id='qsButton_overlay']")
	WebElement SubmiQuoteBtn;

	// Methods that utilize the webelemnts from this page

	public void enterZip(String zip) {
		ZipCodeField.sendKeys(zip);
	}

	public void submitBtn() {
		SubmiQuoteBtn.click();
	}

	public boolean isPageOpened() {
		// Asseted true/flase is page loaded(according to condition)
		return driver.getTitle().contains("Progressive: Ranked One Of The Best Insurance Companies | Progressive");
	}

}
