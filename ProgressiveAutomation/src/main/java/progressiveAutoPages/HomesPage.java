package progressiveAutoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomesPage {

	// Create Object of Webdriver
	public WebDriver driver;

	// Constructor for create an object of this class
	public HomesPage(WebDriver driver) {
		this.driver = driver;
		// Initialize the webelement for this class
		PageFactory.initElements(driver, this);

	}
	// locating all elements that we are using for home page

	// Annotation
	@FindBy(xpath = "//a[@data-qs-product='AU']")
	WebElement autoIcon;

	// Methods that utilize webelement from this page
	public void chooseTypeOfInsurance(String typeOfCoverage) {
		String typeOfCoverageUpperCase = typeOfCoverage.toUpperCase();
		if (typeOfCoverageUpperCase.contains("AUTO")) {
			autoIcon.click();
		} else if (typeOfCoverageUpperCase.contains("AUTO") && typeOfCoverageUpperCase.contains("HOME")) {
			// Click on Auto and click on Renters
		}

	}

	public boolean isPageOpened() {
		// Assert true/false is page loaded(true according to condition)
		return driver.getTitle().contains("Progressive: Ranked One Of The Best Insurance Companies | Progressive");
	}

}
