package progressiveAutoPages;

import org.apache.http.util.Asserts;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class NameAndAddress {
	// Create Instance/objects for WebDriver
	public WebDriver driver;

	// Constructor for create a object of this class
	public NameAndAddress(WebDriver driver) {
		this.driver = driver;

		// Initialize webelement for this class
		PageFactory.initElements(driver, this);

	}
	// Locating all elements for using Enter zip code page

	// Anotation
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']")
	WebElement FirstNameField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_MiddleInitial']")
	WebElement MiddleNameField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	WebElement LastNameField;

	@FindBy(xpath = "//select[@id='NameAndAddressEdit_embedded_questions_list_Suffix']")
	WebElement SuffixField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	WebElement DateOfBirthField;
	
	@FindBy(xpath="//div[contains(text(),'Mailing Address')]")
	WebElement ValidateMailingtext;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']")
	WebElement StreetNoAndNameField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']")
	WebElement AptOrUnitField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_City']")
	WebElement CityField;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_ZipCode']")
	WebElement ZipCodeFld;

	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_MailingZipType']")
	WebElement POBoxField;

	@FindBy(xpath = "//button[contains(text(),'Okay, start my quote')]")
	WebElement StartMyQuoteBtn;

	// Methods that utilize the webelemnts from this page

	public void firstName(String first) {
		FirstNameField.sendKeys(first);

	}
	public void middleName(String middle) {
		MiddleNameField.sendKeys(middle);
	}
	public void lastName(String last) {
		LastNameField.sendKeys(last);
	}
	public void suffixField(String suffix) {
		Select chooseSuffix = new Select(SuffixField);
		chooseSuffix.selectByValue(suffix);
	}
	public void dateOfBirth(String date) {
	
		DateOfBirthField.sendKeys(date);
	}
	
	public String mailTextverfication() {
		String expected =ValidateMailingtext.getText();
		System.out.println(expected);
		return expected;	
		
	}
	public void streetName(String street) {
		StreetNoAndNameField.isDisplayed();
		StreetNoAndNameField.clear();
		StreetNoAndNameField.sendKeys(street);
	}
	
	public void aptNumber(String unit) {
		AptOrUnitField.sendKeys(unit);
	}
	public void cityName(String city) {
		CityField.clear();
		CityField.sendKeys(city);
	}
	public void zipCodes(String zipcod) {
		ZipCodeFld.clear();
		ZipCodeFld.sendKeys(zipcod);
	}
	public void poBox() {
		POBoxField.click();
	}
	public void startQoute() {
		StartMyQuoteBtn.click();
	}

	public boolean isPageOpened() {
		// Asseted true/flase is page loaded(according to condition)
		return driver.getTitle().contains("Name And Address");
	}

}
