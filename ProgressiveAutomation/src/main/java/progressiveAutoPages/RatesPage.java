package progressiveAutoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RatesPage {

	// Create Object of WebDriver
	public WebDriver driver;

	// Constructor for create an object of this class
	public RatesPage(WebDriver driver) {
		this.driver = driver;
		// Initialize the webElement for this class
		PageFactory.initElements(driver, this);

	}
	// locating all elements that we are using for home page

	// Annotation
	@FindBy(xpath = "//button[contains(text(),'Finish & Buy')][1]")
	WebElement FinishAndBuyBtn;

	// Methods that utilize webElement from this page
	public void finishAndBuy() {
		FinishAndBuyBtn.click();
	}
			
	public boolean isPageOpened() {
		// Assert true/false is page loaded(true according to condition)
		return driver.getTitle().contains("Quote");
	}

}
