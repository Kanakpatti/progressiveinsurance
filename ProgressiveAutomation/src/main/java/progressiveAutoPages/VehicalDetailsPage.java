package progressiveAutoPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehicalDetailsPage {

	// create instance/objects for webdriver
	public WebDriver driver;

	// constructor for create a object of this class
	public VehicalDetailsPage(WebDriver driver) {
		this.driver = driver;

		// Initialize webelement for this class
		PageFactory.initElements(driver, this);

	}
	// Locating all elements for using Enter zip code page

	// Anotation
	@FindBy(xpath = "//li[contains(normalize-space(),'2021')]")
	WebElement VehicleYear;

	@FindBy(xpath = "//*[@id=\"VehiclesNew_embedded_questions_list_Make\"]/ul/li[2]")
	WebElement VehicleMake;

	@FindBy(xpath = "//*[@id=\"VehiclesNew_embedded_questions_list_Model\"]/ul/li[2]")
	WebElement VehicleModel;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']")
	WebElement BodyType;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	WebElement PrimaryUse;

	@FindBy(xpath = "//input[@id='VehiclesNew_embedded_questions_list_GaragingZip']")
	WebElement PrimaryZip;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	WebElement OwnOrLease;

	@FindBy(xpath = "//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	WebElement VehicalPeriod;

	@FindBy(xpath = "//input[@id='VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")
	WebElement EmergencyBreak;

	@FindBy(xpath = "//input[@id='VehiclesNew_embedded_questions_list_BlindSpotWarning_Y']")
	WebElement BlindSpot;

	@FindBy(xpath = "//*[@id=\"Div1\"]/forward-navigation/loading-button/button")
	WebElement ContinueBtn;

	// Methods that utilize the webelemnts from this page

	public void vehicalYear() {
		VehicleYear.click();
	}

	public void vehicalMake() {
		VehicleMake.click();
	}

	public void vehicleModel() {
		VehicleModel.click();
	}

	public void bodyType(String type) {
		Select chooseType = new Select(BodyType);
		chooseType.selectByValue(type);
	}

	public void primaryUse(String use) {
		Select chooseType = new Select(PrimaryUse);
		chooseType.selectByVisibleText(use);
	}

	public void primaryZip(String zip) {
	
	PrimaryZip.sendKeys(zip);
	}
	public void ownOrLease(String status) {
		Select chooseStatus = new Select(OwnOrLease);
		chooseStatus.selectByVisibleText(status);
	}
	public void vehicalPeriod(String period) {
		Select choosePeriod = new Select(VehicalPeriod);
		choosePeriod.selectByVisibleText(period);
	}
	public void emergencyBreak() {
		EmergencyBreak.click();
		
	}
	public void blindSpot() {
		BlindSpot.click();
	}
	public void continueBtn() {
		ContinueBtn.click();
	}
	public boolean isPageOpened() {
		// Asseted true/flase is page loaded(according to condition)
		return driver.getTitle().contains("Edit Vehicle");
	}	
}
