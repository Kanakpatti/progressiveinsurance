package progressiveAutoTests;

import java.util.concurrent.TimeUnit;
import static org.testng.Assert.assertEquals;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import SeleniumUtil.ScreenShotUtil;

import junit.framework.Assert;
import progressiveAutoPages.HomesPage;

public class StartAutoQuote {
	// Global Variable
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {

		// Before test setup
		beforeTest();

		// the actual test page objects and test case
		startQuote();

		// Terminate the browser
		afterTest();
	}

	public static void beforeTest() throws InterruptedException {
		// Step 1 : Set the system path

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Step 2: Use desired driver
		driver = new ChromeDriver();

		Thread.sleep(3000);

	}

	public static void startQuote() {
		try {
			// Step 2: Create a home page for desire elements
			HomesPage homePage = new HomesPage(driver);

			ScreenShotUtil util = new ScreenShotUtil();

			// Step 3: Navigate to Progessive page
			driver.get("https://www.progressive.com/home/home/");
			// Step 4: Maximize the screen
			driver.manage().window().maximize();
			// Wait for page load
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Assert.assertTrue(homePage.isPageOpened());
			// Take Snapshot
			util.captureSnapShot(driver, "Progessive HomePage");
//			// Enter the zip code
//			homePage.enterZip("75038");
//			Thread.sleep(2000);
//			// Enter on start Quote
//			homePage.submitBtn();

//			// Validate if type of insurance page is opened
//			typeOfInsurance.isPageOpened();
//			Thread.sleep(3000);
//
//			// Take screenshot of new page
//			util.captureSnapShot(driver, "Different kind of Insurance Coverage Page");

			// Choose type of Insurance
			homePage.chooseTypeOfInsurance("Auto");
			Thread.sleep(3000);
			// Take screenshot of next page
			util.captureSnapShot(driver, "Details Page");

		} catch (org.openqa.selenium.NoSuchElementException e) {
			e.getAdditionalInformation();
			e.getMessage();
			e.getStackTrace();
			e.getCause();
		} catch (Exception e) {
			e.getStackTrace();
			e.printStackTrace();
			e.getCause();
			e.getMessage();
		}

	}

	public static void afterTest() {
		// Close all windows
		driver.close();
		// Quite the session
		driver.quit();

	}

}
