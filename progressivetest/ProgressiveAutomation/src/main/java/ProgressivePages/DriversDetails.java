package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DriversDetails {
	
	// Create Instance/objects for WebDriver
		public WebDriver driver;

		// Constructor for create a object of this class
		public DriversDetails(WebDriver driver) {
			this.driver = driver;

			// Initialize webelement for this class
			PageFactory.initElements(driver, this);

		}
		// Locating all elements for using Enter zip code page

		// Anotation
		@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']")
		WebElement GenderField;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']")
		WebElement MaritalStatus;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']")
		WebElement EducationLevel;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']")
		WebElement EmploymentStatus;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']")
		WebElement PrimaryResident;
		
		@FindBy(xpath="//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']")
		WebElement MovedStatus;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']")
		WebElement LicenseStatus;

		@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']")
		WebElement YearOfLicense;

		@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
		WebElement AccidentClaim;

		@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
		WebElement PoliceTickets;


		@FindBy(xpath = "//button[contains(text(),'Continue')]")
		WebElement ContinueBtn;

		// Methods that utilize the webelemnts from this page

		public void genderField() {
			GenderField.click();

		}
		public void maritalStatus(String status) {
			Select chooseStatus = new Select(MaritalStatus);
			chooseStatus.selectByVisibleText(status);
		}
		public void educationLevel(String level) {
			Select chooseLevel = new Select(EducationLevel);
			chooseLevel.selectByVisibleText(level);
		}
		public void employmentStatus(String status) {
			Select chooseStatus = new Select(EmploymentStatus);
			chooseStatus.selectByVisibleText(status);
		}
		public void primaryResidence(String residence) {
			Select chooseResidence = new Select(PrimaryResident);
			chooseResidence.selectByVisibleText(residence);
		}
		public void movedStatus(String status) {
			Select chooseStatus = new Select(MovedStatus);
			chooseStatus.selectByVisibleText(status);
		}
		public void licenseStatus(String status) {
			Select chooseStatus = new Select(LicenseStatus);
			chooseStatus.selectByVisibleText(status);
		}
		public void yearOfLicense(String year) {
			Select chooseYearOfLicense = new Select(YearOfLicense);
			chooseYearOfLicense.selectByVisibleText(year);
		}
		public void accidentClaim() {
			AccidentClaim.click();
		}
		
		public void policeTicket() {
			PoliceTickets.click();
		}
		public void continueBtn() {
			ContinueBtn.click();
		}

		public boolean isPageOpened() {
			// Asseted true/flase is page loaded(according to condition)
			return driver.getTitle().contains("Policyholder Details");
		}


}
