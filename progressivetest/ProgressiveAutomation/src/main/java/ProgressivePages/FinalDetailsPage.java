package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FinalDetailsPage {
	
	// Create Instance/objects for WebDriver
			public WebDriver driver;

			// Constructor for create a object of this class
			public FinalDetailsPage (WebDriver driver) {
				this.driver = driver;

				// Initialize webelement for this class
				PageFactory.initElements(driver, this);

			}
			// Locating all elements for using Final Details page

			// Anotation
			@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceToday_N']")
			WebElement TodaysInsurance;

			@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceLastMonth_N']")
			WebElement InsuranceWithinMonth;

			@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_OtherPolicies_N']")
			WebElement InsuranceHistory;

			@FindBy(xpath = "//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
			WebElement EmailAddress;
			
			@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_TotalResidents']")
			WebElement HomeResidents;

			@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_CurrentResidence']")
			WebElement PeriodOfResident;
			
			@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_TotalPipClaimsCount']")
			WebElement InjuryClaim;

			@FindBy(xpath = "//button[contains(text(),'Continue')]")
			WebElement ContinueBtn;

			@FindBy(xpath = "//input[@id='SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N']")
			WebElement SnapshotPage;
			
			// Methods that utilize the webelemnts from this page

			public void todayInsurance() {
				TodaysInsurance.click();

			}
			public void insuranceWithinMonth() {
				InsuranceWithinMonth.click();
			}
			public void insuranceHistory() {
				InsuranceHistory.click();
			}
			public void emailAddress(String email) {
				EmailAddress.sendKeys(email);
			}
			public void homeResidents(String number) {
				Select chooseResident = new Select(HomeResidents);
				chooseResident.selectByValue(number);
			}
			public void periodOfResident(String period) {
				Select choosePeriod = new Select(PeriodOfResident);
				choosePeriod.selectByVisibleText(period);
			}
			public void injuryClaim(String claim) {
				Select chooseClaim = new Select(InjuryClaim);
				chooseClaim.selectByValue(claim);
			}
			public void continueBtn() {
				ContinueBtn.click();
			}
			public void snapShot() {
				SnapshotPage.click();
			}

			public boolean isPageOpened() {
				// Asseted true/flase is page loaded(according to condition)
				return driver.getTitle().contains("Additional Details");
			}



}
