package ProgressiveTests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class GoogleSearchTestNg {
	WebDriver driver;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;

	@BeforeTest
	public void BeforeTest() {

		htmlReporter = new ExtentHtmlReporter("extentReporterTestNgGoogle.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// set system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		// Invoke the Web browser
		driver.get("https://www.google.com");

		// Max the Webdriver
		driver.manage().window().maximize();

	}

	@Test(priority = 0)
	public void googleSearch() {

		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// step3

		ExtentTest test = extent.createTest("Google search Test", "This is a Google search test");
		test.info("Executing Google Quote Test....");
		

		if (driver.getTitle().contains("Google")) {
			test.pass("Navigate to Google Pages");
		}
		
		test.log(Status.INFO, "Ending Google Test");
		extent.flush();

		// Creating Webelement Object and locating the element
		WebElement searchField = driver.findElement(By.xpath("//input[@name='q']"));

		// Interacting with element by sending keys or text
		searchField.sendKeys("What is TestNg");

		// Pressing Enter on the keyboard
		searchField.sendKeys(Keys.RETURN);

	}

	@AfterTest
	public void afterTest() {

		// Close the Browser
		driver.close();
		// Quite the Driver
		driver.quit();
	}

}
