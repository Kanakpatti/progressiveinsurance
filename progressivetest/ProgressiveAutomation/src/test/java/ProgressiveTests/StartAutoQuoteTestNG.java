package ProgressiveTests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import ProgressivePages.DriversDetails;
import ProgressivePages.EnterZipCode;
import ProgressivePages.FinalDetailsPage;
import ProgressivePages.HomesPage;
import ProgressivePages.NameAndAddress;
import ProgressivePages.RatesPage;
import ProgressivePages.VehicalDetailsPage;
import SeleniumUtil.ScreenShotUtil;
import junit.framework.Assert;

public class StartAutoQuoteTestNG {

	WebDriver driver;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;

	@BeforeTest
	public void BeforeTest() {

		htmlReporter = new ExtentHtmlReporter("extentReporterTestNgQuote.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// set system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void navigateToHomesPage() {

		try {
			HomesPage homePage = new HomesPage(driver);
			ScreenShotUtil util = new ScreenShotUtil();

			// Navigate to Progressive Page
//			ExtentTest test = extent.createTest("Progressive Quote Test", "This is a progressive quote test");
//			test.info("Executing Progressive Quote Test....");
//			driver.get("https://www.progressive.com/home/home/");
//			
//			if (driver.getTitle().contains("Progressive: Ranked One Of The Best Insurance Companies | Progressive")) {
//				test.pass("Navigate to Progressive Pages");
//			}
//			// Maximize the Screen
//			driver.manage().window().maximize();
////			test.log(Status.INFO, "Ending Progressive Test");
//			// wait for page load
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			Assert.assertTrue(homePage.isPageOpened());
//			// Take a snap shot
//			util.captureSnapShot(driver, "Progerssive HomePage");
//			
//			ExtentTest test2 = extent.createTest("Google page Test", "This is a simple Google test");
//			test2.info("Executing Google Test....");
//			driver.get("https://www.google.com");
//			
//			if (driver.getTitle().contains("Google")) {
//				test2.pass("Navigate to Google Homepage");
//			}
//			driver.manage().window().maximize();
//			test2.log(Status.INFO, "Ending Google Test");
//			
//			test2.fail("This is test to check fail status");
//			extent.flush();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
			e.getMessage();

		}
	}

	@Test(priority = 0)
	public void startAutoQuote() {
		navigateToHomesPage();

		// step2

		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// step3

		ExtentTest test = extent.createTest("Progressive Quote Test", "This is a progressive quote test");
		test.info("Executing Progressive Quote Test....");
		driver.get("https://www.progressive.com/home/home/");
////		driver.get("https://www.google.com");
		
		if (driver.getTitle().contains("Progressive: Ranked One Of The Best Insurance Companies | Progressive")) {
			test.pass("Navigate to Progressive Pages");
		}
		driver.manage().window().maximize();
		test.log(Status.INFO, "Ending Progressive Test");
		extent.flush();

		try {
			HomesPage homePage = new HomesPage(driver);
			EnterZipCode zipCode = new EnterZipCode(driver);
			NameAndAddress fillForm = new NameAndAddress(driver);
			VehicalDetailsPage vehicalDetails = new VehicalDetailsPage(driver);
			DriversDetails driverInfo = new DriversDetails(driver);
			FinalDetailsPage finalDetails = new FinalDetailsPage(driver);
			RatesPage quoteRate = new RatesPage(driver);
			ScreenShotUtil util = new ScreenShotUtil();

			// Choose type of Insurance
			homePage.chooseTypeOfInsurance("Auto");
			Thread.sleep(2000);
			// take a screenshot of next page
			util.captureSnapShot(driver, "Enter Zipcode page");

			// Enter Zip Code
			zipCode.enterZip("75038");

			// Click on Start Quote
			zipCode.submitBtn();
			Thread.sleep(3000);

			// Take a screenshot of Name and Address page
			util.captureSnapShot(driver, "Name and Address page");

			// Fill a Name and Address form

			// First Name
			fillForm.firstName("Samundra");
			Thread.sleep(2000);
			// Middle Name
			fillForm.middleName("Kumar");
			Thread.sleep(2000);
			// Last Name
			fillForm.lastName("Kharel");
			Thread.sleep(2000);

			// Suffix Field
			fillForm.suffixField("JR");
			Thread.sleep(2000);

			// Date of Birth
			fillForm.dateOfBirth("10/06/1979");
			Thread.sleep(2000);
			// Mailing Text
			fillForm.mailTextverfication();

			// Apt no
			fillForm.aptNumber("1024");
			Thread.sleep(2000);

			// Street Address
			fillForm.streetName("123 test address");
			Thread.sleep(2000);
			// Apt no
			fillForm.aptNumber("1024");
			Thread.sleep(2000);
			// City Name
			fillForm.cityName("Irving");
			Thread.sleep(2000);
			// Zip Code
			fillForm.zipCodes("75038");
			Thread.sleep(2000);
			// PO Box select
			fillForm.poBox();
			Thread.sleep(2000);
			// Submit for Start Quote
			fillForm.startQoute();
			Thread.sleep(2000);

			// Take a Screenshot of New Page after fill the form
			util.captureSnapShot(driver, "New Page after fill the form");

			// Fill the Vehicle Details
			vehicalDetails.vehicalYear();
			Thread.sleep(2000);
			vehicalDetails.vehicalMake();
			Thread.sleep(2000);
			vehicalDetails.vehicleModel();
			Thread.sleep(2000);
			vehicalDetails.bodyType("4DR 4CYL");
			Thread.sleep(2000);
			vehicalDetails.primaryUse("Personal (to/from work or school, errands, pleasure)");
			Thread.sleep(2000);
			vehicalDetails.primaryZip("75038");
			Thread.sleep(2000);
			vehicalDetails.ownOrLease("Finance");
			Thread.sleep(2000);
			vehicalDetails.vehicalPeriod("1 month - 1 year");
			Thread.sleep(2000);
			vehicalDetails.emergencyBreak();
			Thread.sleep(2000);
			vehicalDetails.blindSpot();
			Thread.sleep(2000);
			vehicalDetails.continueBtn();
			Thread.sleep(4000);
			// Take a screenshot of Name and Address page
			util.captureSnapShot(driver, "Driver Details Page");

			// Fill the Drivers Details

			driverInfo.genderField();
			Thread.sleep(2000);
			driverInfo.maritalStatus("Single");
			Thread.sleep(2000);
			driverInfo.educationLevel("High school diploma or GED");
			Thread.sleep(2000);
			driverInfo.employmentStatus("Employed");
			Thread.sleep(2000);
			driverInfo.occupation("Quality Assurance Tester");
			driverInfo.primaryResidence("Rent");
			Thread.sleep(2000);
			driverInfo.movedStatus("No");
			Thread.sleep(2000);
			driverInfo.licenseStatus("Valid");
			Thread.sleep(2000);
			driverInfo.yearOfLicense("3 years or more");
			Thread.sleep(2000);
			driverInfo.accidentClaim();
			Thread.sleep(2000);
			driverInfo.policeTicket();
			Thread.sleep(2000);
			driverInfo.continueBtn();
			Thread.sleep(4000);
			driverInfo.continueBtn();
			Thread.sleep(5000);
			driverInfo.continueBtn();
			Thread.sleep(4000);

			// Take a screenshot of Name and Address page
			util.captureSnapShot(driver, " Final Details Page");

			// Fill the Final Details Page
			finalDetails.todayInsurance();
			Thread.sleep(2000);
			finalDetails.insuranceWithinMonth();
			Thread.sleep(2000);
			finalDetails.insuranceHistory();
			Thread.sleep(2000);
			finalDetails.emailAddress("test@tester.com");
			Thread.sleep(2000);
			finalDetails.homeResidents("4");
			Thread.sleep(2000);
			finalDetails.periodOfResident("More than 1 year");
			Thread.sleep(2000);
			finalDetails.injuryClaim("0");
			Thread.sleep(2000);
			finalDetails.continueBtn();
			Thread.sleep(4000);
			finalDetails.snapShot();
			Thread.sleep(2000);
			finalDetails.continueBtn();
			Thread.sleep(4000);
			finalDetails.noThanks();
			Thread.sleep(4000);

			// Take a screenshot of Final Details page
			util.captureSnapShot(driver, " Validation Page");

			// Find the Final Rates
			quoteRate.finishAndBuy();
			Thread.sleep(3000);
			// Take a screenshot of Final Details page
			util.captureSnapShot(driver, " Final Quote Rates");
			
			
			
			ExtentTest test2 = extent.createTest("Google page Test", "This is a simple Google test");
			test2.info("Executing Google Test....");
			driver.get("https://www.google.com");
			
			if (driver.getTitle().contains("Google")) {
				test2.pass("Navigate to Google Homepage");
			}
			driver.manage().window().maximize();
			test2.log(Status.INFO, "Ending Google Test");
			
			test2.fail("This is test to check fail status");
			extent.flush();

		} catch (org.openqa.selenium.NoSuchElementException e) {
			e.getAdditionalInformation();
			e.getMessage();
			e.getStackTrace();
			e.getCause();

		} catch (Exception e) {
			e.getStackTrace();
			e.printStackTrace();
			e.getCause();
			e.getMessage();

		}
	}

	@AfterTest
	public void afterTest() {

		// Close the Browser
		driver.close();
		// Quite the Driver
		driver.quit();
	}

}
